#include <stdlib.h>

int* criaVetor(int tam);
void preencheVetor(int* vetor, int tam, int valor);
void imprimeVetor(int* vetor, int tam);

int* criaVetor1(int tam){
    int* v = (int*)calloc(tam, sizeof(int));
    return v;
}

void criaVetor2(int tam, int** saida){
    int* v = (int*)calloc(tam, sizeof(int));
    *saida = v;
}

void preencheVetor(int* vetor, int tam, int valor){
    for (int i=0; i < tam; i++){
        vetor[i] = valor;
    }
}

void imprimeVetor(int* vetor, int tam){
    printf("[");
    for (int i=0; i < tam; i++){
        printf("%d", vetor[i]);
        if(i < tam-1) printf(",");
    }
    printf("]");
}

