#include<stdio.h>
#include "vetores.c"

int main(){
    
    // Criar um vetor
    int* v1 = criaVetor1(10);
    int* v2;
    criaVetor2(10, &v2);

    // Preencher o vetor
    preencheVetor(v1, 10, -55);
    preencheVetor(v2, 5, 99);

    // Imprimir o vetor
    imprimeVetor(v1, 10); // [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]
    printf("\n");
    imprimeVetor(v2, 5);  // [99,99,99,99,99]

    return 0;
}