#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>

int dobra(int n){
    return n*2;
}

int incrementa(int n){
    return n+1;
}




int* criaVetor(int tam){
    int* v = (int*)calloc(tam, sizeof(int));
    return v;
}

void imprimeVetor(int* vetor, int tam){
    printf("[");
    for (int i=0; i < tam; i++){
        printf("%d", vetor[i]);
        if(i < tam-1) printf(",");
    }
    printf("]\n");
}

int* map(int* v, int tam, int (*funcao)(int)){
    int* novo = (int*) calloc(tam, sizeof(int));

    for (int i=0; i < tam; i++){
        novo[i] = funcao(v[i]);
    }
    return novo;
    

}


int main(){
    
    int* v = criaVetor(5);
    v[0] = 10;
    v[1] = 20;
    v[2] = 30;
    v[3] = 40;
    v[4] = 50;
    imprimeVetor(v, 5);

    int* v2 = map(v, 5, &dobra);
    imprimeVetor(v2, 5);

    int* v3 = map(v, 5, &incrementa);
    imprimeVetor(v3, 5);


    return 0;
}