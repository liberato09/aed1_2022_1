#include <stdio.h>
#include <string.h>

#include "fila.h"


void verificar(char* mensagem, bool teste){
    char resultado[10] = teste ? "[PASSOU]" : "[FALHOU]";
    printf("%s - %s\n", resultado, mensagem);
}


int main(){
    
    char str[200];
    TipoElemento elemento;


    /**************************************
    * Teste insercao e primeiro
    **************************************/
    Fila* f1 = fila_criar();
    fila_inserir(f1, 10);
    fila_inserir(f1, 20);
    fila_inserir(f1, 30);
    fila_toString(f1, str);
    verificar("Insercao", strcmp(str, "[10,20,30]")==0);
    

    fila_primeiro(f1, &elemento);
    verificar("Recuperar primeiro elemento", elemento==10);

    /**************************************
    * Teste remocao
    **************************************/
    fila_remover(f1, &elemento);
    verificar("Teste Remocao 1/4", elemento == 10);

    fila_remover(f1, &elemento);
    verificar("Teste Remocao 2/4", elemento == 20);
    
    fila_remover(f1, &elemento);
    verificar("Teste Remocao 3/4", elemento == 30);
    
    bool resultado = fila_remover(f1, &elemento);
    verificar("Teste Remocao 4/4 (fila vazia)", resultado==false);

    fila_destruir(&f1);

       

    /**************************************
    * Teste inserir todos e clone
    **************************************/
    int vetor[12] = {1,2,3,4,5,6,7,8,9,10,11,12};
    f1 = fila_criar();
    fila_inserirTodos(f1, vetor, 12);
    
    fila_toString(f1, str);
    verificar("Teste inserir todos", strcmp(str, "[1,2,3,4,5,6,7,8,9,10,11,12]")==0);

    
    Fila* f2 = fila_clone(f1);
    fila_remover(f2, &elemento);
    fila_remover(f2, &elemento);
    fila_remover(f2, &elemento);
    fila_inserir(f2, 20);
    fila_inserir(f2, 21);
    fila_inserir(f2, 22);

    fila_toString(f2, str);
    verificar("Teste clone", strcmp(str, "[4,5,6,7,8,9,10,11,12,20,21,22]")==0);
    
    fila_destruir(&f1);
    fila_destruir(&f2);

    
    
    

}