#include<stdio.h>
#include<stdlib.h>

int main(){
    int    i = 30;
    double d = 3.5;
    float  f = 2.5;
    char   c = 'j';

    printf("(%u) i: %d\n", &i, i);
    printf("(%u) d: %f\n", &d, d);
    printf("(%u)f: %f\n", &f, f);
    printf("(%u)c: %c\n", &c, c);

    return 0;
}