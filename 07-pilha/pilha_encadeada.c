#include "pilha.h"

/**************************************
* DADOS
**************************************/
typedef struct no{
	TipoElemento dado;
	struct no    *prox;
}No;

struct pilha{
	No *topo;
	int qtdeElementos;
};

/**************************************
* IMPLEMENTAÇÃO
**************************************/
// Desenvolva as funções
Pilha* pilha_criar(){
    Pilha* p = (Pilha*) malloc(sizeof(Pilha));
    p->topo = NULL;
    p->qtdeElementos = 0;
    return p;
}

void pilha_destruir(Pilha** endereco){
    Pilha* p = *endereco;
    
    No* aux = p->topo;
    while()
}


bool pilha_empilhar(Pilha* p, TipoElemento elemento);
bool pilha_desempilhar(Pilha* p, TipoElemento* saida); 
bool pilha_topo(Pilha* p, TipoElemento* saida); 
bool pilha_vazia(Pilha* p);
void pilha_imprimir(Pilha* p);
int pilha_tamanho(Pilha* p);

Pilha* pilha_clone(Pilha* p){
    Pilha* novo = pilha_criar();
    No* aux = p->topo;

    while(aux != NULL){
        pilha_empilhar(novo, aux->dado);
        aux = aux->prox;
    }
    pilha_inverter(novo);
    return novo;
}

void pilha_inverter(Pilha* p){
    if(!pilha_ehValida(p)) return;
    if(pilha_vazia(p)) return;

    Pilha* pilhaAux = pilha_criar();

    TipoElemento elemento = -1;
    while(!pilha_vazia(p)){
        pilha_desempilhar(p, &elemento);
        pilha_empilhar(pilhaAux, elemento);
    }
    p->topo = pilhaAux->topo;
    p->qtdeElementos = pilhaAux->qtdeElementos;
    free(pilhaAux);
}

bool pilha_empilharTodos(Pilha* p, TipoElemento* vetor, int tamVetor);

bool pilha_toString(Pilha* p, char* str){
    str[0] = '\0';
    strcat(str, "[");
    No* aux = p->topo;

    while(aux != NULL){
        char elemento[50];
        sprintf(elemento, "%d", aux->dado);
        
        strcat(str, elemento);
        if(aux->prox != NULL) strcat(str, ",");

        aux = aux->prox;
    }
    
    strcat(str, "]");
}