#include "pilha.h"

/**************************************
* DADOS
**************************************/
typedef struct no{
	TipoElemento dado;
	struct no *prox;
}No;

struct pilha{
	No *topo;
	int qtdeElementos;
};

/**************************************
* IMPLEMENTAÇÃO
**************************************/
// Desenvolva as funções
No* no_Criar(TipoElemento elemento){
  No* novo=(No*)malloc(sizeof(No));
  novo->prox=NULL;
  novo->dado=elemento;
  return novo;
}
Pilha* pilha_criar(){
  Pilha* p=(Pilha*)malloc(sizeof(Pilha));
  p->qtdeElementos=0;
  p->topo=NULL;
  return p;
}
void pilha_destruir(Pilha** endereco){
}
bool pilha_empilhar(Pilha* p, TipoElemento elemento){
  No* novo = no_Criar(elemento);
  novo->prox=p->topo;
  p->qtdeElementos++;
  p->topo=novo;
  return true;
}
bool pilha_desempilhar(Pilha* p, TipoElemento* saida){
  //if(p==NULL)return false;

  printf(".");
  No* no=p->topo;
  p->topo->dado=10;
  p->topo=p->topo->prox;
  
  no->prox = NULL;

  free(no);
  p->qtdeElementos--;
  *saida=p->topo->dado;
  return true;
} 
bool pilha_topo(Pilha* p, TipoElemento* saida){
*saida=p->topo->dado;
  return true;
} 

bool pilha_vazia(Pilha* p){
  if(p->qtdeElementos==0){
    return true;
  }
  return false;
}

void pilha_imprimir(Pilha* p){
  No* topo=(No*)malloc(sizeof(No));
  topo->prox = p->topo;
  topo->dado = p->topo->dado;
  while(p->topo!=NULL){
    printf("%d\n",p->topo->dado);
    p->topo=p->topo->prox;
  }
  p->topo=topo;
}
int pilha_tamanho(Pilha* p){
  return p->qtdeElementos;
}
Pilha* pilha_clone(Pilha* p){
  Pilha* clone = pilha_criar();
  No* topo=(No*)malloc(sizeof(No));
  topo->prox = p->topo;
  while(p->topo->prox!=NULL){
    pilha_inverter(p);
    pilha_empilhar(clone,p->topo->dado);
    p->topo=p->topo->prox;
    }
  return clone;
}
void pilha_inverter(Pilha* p){
  Pilha* invertida = pilha_criar();
  No* topo=(No*)malloc(sizeof(No));
  topo->prox = p->topo;
  while(p->topo->prox!=NULL){
    pilha_empilhar(invertida,p->topo->dado);
    p->topo=p->topo->prox;
  }
  p->topo=topo;
}
bool pilha_empilharTodos(Pilha* p, TipoElemento* vetor, int tamVetor){
  if(tamVetor==0)return false;
  for(int i=0;i<tamVetor;i++){
    pilha_empilhar(p,vetor[i]);
  }
  return true;
}
bool pilha_toString(Pilha* f, char* str){
    *str = '\0';
    strcat(str, "[");
No* topo=(No*)malloc(sizeof(No));
  topo->prox = f->topo;
    for(int i = 0; i <f->qtdeElementos; i++){
        char elemento[20];
        sprintf(elemento, "%d",  f->topo->dado);
        strcat(str, elemento);
        if(i < f-> qtdeElementos-1)strcat(str, ",");
      f->topo=f->topo->prox;
    }
  f->topo=topo;
    printf("]");
    printf("\n");
}
    