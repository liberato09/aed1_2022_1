#include<stdio.h>
#include<stdlib.h>

#include "pilha.h"


int main(){
    
    Pilha* p1 = pilha_criar();



    pilha_empilhar(p1, 10);
    pilha_empilhar(p1, 20);
    pilha_empilhar(p1, 30);
    pilha_imprimir(p1);   // [30,20,10]
    
    int elemento;
    pilha_desempilhar(p1, &elemento);
    printf("%d\n", elemento);           // 30
    pilha_imprimir(p1);                 // [20,10]
    
    char str[200];
    pilha_toString(p1, str);
    printf("%s\n", str);  // [20,10]

    pilha_destruir(&p1);
    printf("%p", p1);          // NULL;

    return 0;
}