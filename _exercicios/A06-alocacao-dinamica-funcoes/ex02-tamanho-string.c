/*****************************************************************************
Escreva uma função que devolva a quantidade de caracteres de uma string.

- ***Não é permitido utilizar funções da biblioteca string.h****.

Desenvolva 2 versões dessa função:

- tamanhoString1: usando a saída com **return**.
- tamanhoString2: usando a saída com a **estratégia do scanf**.
*****************************************************************************/

#include <stdio.h>
#include <stdbool.h>



int tamanhoString1(char* str){
    if(str == NULL) return -1;

    int cont = 0;
    while(str[cont] != '\0') cont++;
    
    return cont;
}

bool tamanhoString2(char* str, int* endSaida){
    int tam = tamanhoString1(str);
    if (tam == -1) return false;

    *endSaida = tam;
    return true;
}

int main(){

    // ENTRADA
    char texto1[] = "Confie no Senhor de todo o seu coração e não se apoie em seu próprio entendimento. Provérbios 3:5";
    char texto2[100] = "As mentes mais poderosas têm objetivos, as demais, têm desejos";
    


    // PROCESSAMENTO
    int tamTexto1, tamTexto2;

    tamTexto1 = tamanhoString1(texto1);
    tamanhoString2(texto2, &tamTexto2);


    // SAÍDA
    printf("(%d) %s\n", tamTexto1, texto1);
    printf("(%d) %s\n", tamTexto2, texto2);
    
    return 0;
}