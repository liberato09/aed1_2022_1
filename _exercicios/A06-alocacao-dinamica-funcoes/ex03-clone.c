/*****************************************************************************
Escreva uma função para duplicar vetores de inteiro. A função deve ser capaz 
de replicar o conteúdo do vetor em outra região de memória. 

Desenvolva 2 versões dessa função:

- clonarVetor1: usando a saída com **return**.
- clonarVetor2: usando a saída com a **estratégia do scanf**.
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int* clonarVetor1(int* vetor, int tam){
    int* clone = (int*) calloc(tam, sizeof(int));
    if(clone == NULL) return NULL;

    // for (int i=0; i < tam; i++){
    //     clone[i] = vetor[i];
    // }
    memcpy(clone, vetor, tam * sizeof(int));
    return clone;
}

bool clonarVetor2(int* vetor, int tam, int** endVetor){
    int* clone = clonarVetor1(vetor, tam);
    if(clone == NULL) return false;

    *endVetor = clone;

    return true;
}

void imprimeVetor(int* vetor, int tam){
    printf("[");
    for (int i=0; i < tam; i++){
        printf("%d", vetor[i]);
        if(i < tam-1) printf(",");
    }
    printf("]");
    printf("\n");
}


int main(){

    // ENTRADA
    int v1[5] = {1,2,3,4,5};
    int v2[8] = {1,2,3,4,5,6,7,8};

    // PROCESSAMENTO
    int* clone1;
    int* clone2;

    clone1 = clonarVetor1(v1, 5);
    clonarVetor2(v2, 8, &clone2);

    // SAÍDA
    clone1[0] = -1;
    imprimeVetor(v1, 5);
    imprimeVetor(clone1, 5);

    clone2[0] = -100;
    imprimeVetor(v2, 8);
    imprimeVetor(clone2, 8);
    
    return 0;
}