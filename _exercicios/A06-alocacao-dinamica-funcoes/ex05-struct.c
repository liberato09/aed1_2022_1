/*****************************************************************************
a) Crie uma função responsável por criar dinamicamente um produto e inicializar 
seus valores. Os valores devem ser recebidos como parâmetro. (`criaProduto`)

b) Crie uma função responsável por imprimir um produto. A função deve imprimir 
os valores de todos os atributos do produto. (`imprimeProduto`)

c) As duas declarações abaixo criam um vetor de produtos. Analise as duas formas 
e descreva as vantagens e desvantagens de cada uma.
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>


typedef struct produto{
	unsigned int codigo;
	char nome[50];
	float preco;
}Produto;


Produto* criaProduto(int codigo, char* nome, float preco){
    Produto* novo = (Produto*) malloc(sizeof(Produto));
    novo->codigo = codigo;
    strcpy(novo->nome, nome);
    novo->preco = preco;
    return novo;
}

void imprimirProduto(Produto* p){
    printf("(");
    printf("%d, %s, %.2f", p->codigo, p->nome, p->preco);
    printf(")");
    printf("\n");
}

int main(){

    // (a)
    Produto* p1 = criaProduto(1, "Mouse Logitech", 150.00);
    Produto* p2 = criaProduto(2, "Monitor LG 32pol", 1200.00);

    imprimirProduto(p1);
    imprimirProduto(p2);
    
    


    return 0;
}