/*****************************************************************************
Escreva uma função para criar vetores de inteiros de diferentes tamanhos 
preenchidos com valores aleatórios. 

Desenvolva 2 versões dessa função:

- criaVetorAleatorio1: usando a saída com *return*.
- criaVetorAleatorio2: usando a saída com a *estratégia do scanf*.
*****************************************************************************/

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

int* criaVetorAleatorio1(int tam){
    int* vetor = (int*)calloc(tam, sizeof(int));
    if (vetor == NULL) return NULL;

    // troca a semente utilizada na geração dos números aleatórios
    srand((long)vetor);
    for (int i=0; i < tam; i++){
        vetor[i] = rand() % (100);
    }
    return vetor;    
}

bool criaVetorAleatorio2(int tam, int** endVetor){
    int* vetor = criaVetorAleatorio1(tam);
    if(vetor == NULL) return false;

    *endVetor = vetor;
    return true;
}


void imprimeVetor(int* vetor, int tam){
    printf("[");
    for (int i=0; i < tam; i++){
        printf("%d", vetor[i]);
        if(i < tam-1) printf(",");
    }
    printf("]");
    printf("\n");
}

int main(){
    // inicializa as variáveis que armazenarão os vetores
    int* v1;
    int* v2;
    
    // ENTRADA
    int tamV1 = 10;
    int tamV2 = 20;

    // PROCESSAMENTO
    v1 = criaVetorAleatorio1(tamV1);
    criaVetorAleatorio2(tamV2, &v2);

    // SAÍDA
    imprimeVetor(v1, tamV1);
    imprimeVetor(v2, tamV2);

    return 0;
}