/*****************************************************************************
Escreva uma função que seja capaz de dobrar o tamanho de um vetor. A função 
deve preservar os valores do vetor e preencher com zero as posições adicionais 
e devolver o endereço 

O nome da função deve ser `dobrar`. A função somente funciona para vetores alocados dinamicamente.

*Entrada:*

- endereço de memória da variável que contém o endereço do vetor
- novo tamanho do vetor

*Saída:*

- bool - representando sucesso ou fracasso.
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>


int dobrar(int** endVetor, int tam){
    int novoTam = tam * 2;
    int* temp = (int*)calloc(novoTam, sizeof(int));
    if(temp == NULL) return -1;

    int* vetor = *endVetor;

    for (int i=0; i < tam; i++){
        temp[i] = vetor[i];        
    }

    for (int i=tam; i < novoTam; i++){
        temp[i] = 0;        
    }

    free(vetor);
    *endVetor =  temp;

    return novoTam;
}



int* criaVetorAleatorio1(int tam){
    int* vetor = (int*)calloc(tam, sizeof(int));
    if (vetor == NULL) return NULL;

    // troca a semente utilizada na geração dos números aleatórios
    srand (time (NULL));
    for (int i=0; i < tam; i++){
        vetor[i] = rand() % (tam * 10);
    }
    return vetor;    
}

void imprimeVetor(int* vetor, int tam){
    printf("[");
    for (int i=0; i < tam; i++){
        printf("%d", vetor[i]);
        if(i < tam-1) printf(",");
    }
    printf("]");
    printf("\n");
}


int main(){

    
    int* v1 = criaVetorAleatorio1(10);
    imprimeVetor(v1, 10);

    int novoTam = dobrar(&v1, 10);
    printf("Novo tamanho: %d\n", novoTam);
    imprimeVetor(v1, novoTam);


    return 0;
}