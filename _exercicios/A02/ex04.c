#include<stdio.h>
#include<string.h>
#include<stdbool.h>


bool extrai(char origem[], int posInicial, int posFinal, char destino[]){
    if (posInicial < 0)             return false;
    if (posFinal >= strlen(origem)) return false;
    if (posFinal < posInicial)      return false;

    int i,j;
    for (i=posInicial, j = 0; i <= posFinal; i++, j++){
        destino[j] = origem[i];
    }
    destino[j] = '\0';
    return true;
}


int main(){
    
    // ENTRADA  
    // char string[100] = "foto_aniversario.jpg";
    // int posInicial = 17;
    // int posFinal = 19;

    char string[100];
    int posInicial;
    int posFinal;

    FILE *arqEntrada = fopen("ex04.in", "r");    
    fgets(string, 100, arqEntrada);   
    fscanf(arqEntrada, "%d %d", &posInicial, &posFinal);
    fclose(arqEntrada);

    
    // PROCESSAMENTO
    char substring[100];
    bool resultado = extrai(string, posInicial, posFinal, substring);

    
    // SAIDA
    printf("%s\n", substring);

    return 0;
}