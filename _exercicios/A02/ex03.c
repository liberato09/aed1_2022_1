#include<stdio.h>
#include<stdbool.h>
#include<string.h>


bool ehVogal(char c){
    char vogais[] = "aeiouAEIOU";
    int tam = strlen(vogais);
    for (int i=0; i < tam; i++){
        if (c == vogais[i]) return true;
    }
    return false;
}

int contaVogais(char str[]){
    int qtdeVogais = 0;
    int tam = strlen(str);
    for (int i=0; i < tam; i++){
        if (ehVogal(str[i])) qtdeVogais++;
    }
    return qtdeVogais;
}


int main(){

    // ENTRADA
    char str[100];        
    FILE *arqEntrada = fopen("ex03.in", "r");    
    fgets(str, 100, arqEntrada);    
    fclose(arqEntrada);

    // PROCESSAMENTO
    int qtdeVogais = contaVogais(str);
    

    // SAIDA
    printf("%s\n", str);
    printf("Quantidade vogais: %d\n", qtdeVogais);

    return 0;
}