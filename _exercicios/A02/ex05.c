
struct horario{
    int hora;
    int min;
    int seg;
};

struct data{
    int dia;
    int mes;
    int ano;
};

struct compromisso{
    struct horario horario;
    struct data data;
    char texto[200];
};

int main(){
    

    struct horario h;
    h.hora = 10;
    h.min = 15;
    h.seg = 45;

    struct data d;
    d.dia = 14;
    d.mes = 3;
    d.ano = 2022;

    struct compromisso c;
    c.horario = h;
    c.data = d;
    strcpy(c.texto, "Aula AED1");

}