#include<stdio.h>
#include<stdbool.h>

int somaNumeros(int vet[], int tam){
    int soma = 0;
    for (int i=0; i < tam; i++){
        soma += vet[i];
    }
    return soma;
    
}

int main(){
    
    // ENTRADA
    int vetor[5];
    
    FILE *arqEntrada = fopen("ex02.in", "r");
    for (int i=0; i < 5; i++){
        fscanf(arqEntrada, "%d", &vetor[i]);        
    }
    fclose(arqEntrada);

    // PROCESSAMENTO
    int soma = somaNumeros(vetor, 5);

    // SAIDA
    
    printf("Soma do vetor %d \n", soma);
    
    


    return 0;
}