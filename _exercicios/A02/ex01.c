#include<stdio.h>
#include<stdbool.h>

bool buscaNumero(int n, int vet[], int tam){
    
    for (int i=0; i < tam; i++){
        if(vet[i] == n){
            return true;
        }
    }
    return false;
    
}

int main(){
    
    // ENTRADA
    // int vetor[5] = {2,4,6,8,10};
    // int num = 20;
    int vetor[5];
    int num;
    
    FILE *arqEntrada = fopen("ex01.in", "r");
    for (int i=0; i < 5; i++){
        fscanf(arqEntrada, "%d", &vetor[i]);        
    }
    fscanf(arqEntrada, "%d", &num);        
    fclose(arqEntrada);

    // PROCESSAMENTO
    bool encontrado = buscaNumero(num, vetor, 5);

    // SAIDA
    if(encontrado)
        printf("Numero %d encontrado\n", num);
    else
        printf("Numero %d NAO encontrado\n", num);


    return 0;
}