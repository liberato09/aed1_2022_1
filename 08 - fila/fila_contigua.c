

#include "fila.h"

#define TAM_INICIAL 5


/**************************************
* ESPECIFICAÇÃO DOS DADOS
**************************************/
struct fila{
    TipoElemento* vetor;
	int tamVetor;
	int inicio;
	int fim;
	int qtdeElementos;
};
/**************************************
* FUNÇÕES AUXILIARES
**************************************/

bool fila_ehValida(Fila* f){
    if (f == NULL) return false;
    if (f->vetor == NULL) return false;

    return true;
}

bool realoca(Fila* f, int novoTamanho){
    TipoElemento* vetorAuxiliar = (TipoElemento*)calloc(novoTamanho, sizeof(TipoElemento));
    if (vetorAuxiliar == NULL) return false;

    int i;
    int pos = f->inicio;

    // cópia dos elementos
    for(i = 0; i < f->qtdeElementos; i++){
        vetorAuxiliar[i] = f->vetor[pos];
        pos = (pos+1) % f->tamVetor;
    }
    // desalocar o vetor original
    free(f->vetor);
    // atualizar o vetor para o novo espaço
    f->vetor = vetorAuxiliar;
    f->tamVetor = novoTamanho;
    // atualizar as variáveis de controle
    f->inicio = 0;
    f->fim = f->qtdeElementos;
    return true;
}



/**************************************
* IMPLEMENTAÇÃO
**************************************/
Fila* fila_criar(){
    Fila* f = (Fila*) malloc(sizeof(Fila));
    f->vetor = (TipoElemento*) calloc(TAM_INICIAL, sizeof(TipoElemento));
    f->tamVetor = TAM_INICIAL;
    f->qtdeElementos = 0;
    f->inicio = f->fim = 0;
    return f;    
}


void fila_destruir(Fila** enderecoFila){
    if (enderecoFila == NULL) return;
    if(!fila_ehValida(*enderecoFila)) return;

    Fila* f = *enderecoFila;
    free(f->vetor);
    free(f);
    *enderecoFila = NULL;
}

bool fila_inserir(Fila* f, TipoElemento elemento){
    if(!fila_ehValida(f)) return false;
    // se a fila estiver cheia
    if(f->qtdeElementos == f->tamVetor){
        if (!realoca(f, f->tamVetor * 2)) return false;
    }

    f->vetor[f->fim] = elemento;
    f->fim = (f->fim + 1) % f->tamVetor;
    f->qtdeElementos++;

    return true;

}

bool fila_remover(Fila* f, TipoElemento* saida){ // estratégia do scanf
    if(!fila_ehValida(f)) return false;
    if(fila_vazia(f)) return false;

    TipoElemento elemento = f->vetor[f->inicio];
    f->inicio = (f->inicio+1) % f->tamVetor;
    *saida = elemento;
    f->qtdeElementos--;
    
    // se os elementos estiverem ocupando menos do que 25% da capacidade
    // reduz pela metade o vetor
    if(f->qtdeElementos / f->tamVetor < 0.25){
        realoca(f, f->tamVetor / 2);
    }
    
    return true;
}
bool fila_primeiro(Fila* f, TipoElemento* saida){ // estratégia do scanf
    if(!fila_ehValida(f)) return false;
    if(fila_vazia(f)) return false;

    *saida = f->vetor[f->inicio];
    return true;
}

bool fila_vazia(Fila* f){
    if(!fila_ehValida(f)) return true;

    return (f->qtdeElementos == 0 ? true : false);
}
int fila_tamanho(Fila* f){
    if(!fila_ehValida(f)) return 0;

    return f->qtdeElementos;
}
void fila_imprimir(Fila* f){
    if (!fila_ehValida(f)) return;
    
    printf("[");
    int i = f->inicio;
    while(i != f->fim){
        printf("%d", f->vetor[i]);
        if ((i+1)%f->tamVetor != f->fim) printf(",");
        i = (i+1) % f->tamVetor;
    }
    printf("]");
}
Fila* fila_clone(Fila* f){
    if(!fila_ehValida(f)) return NULL;

    Fila* clone = fila_criar();
    int i = f->inicio;
    while(i != f->fim){
        fila_inserir(clone, f->vetor[i]);        
        i = (i+1) % f->tamVetor;
    }
    return clone;
}
bool fila_toString(Fila* f, char* str){
    if (!fila_ehValida(f)) return false;

    str[0] = '\0';
    strcat(str, "[");
    char elemento[20];

    int i = f->inicio;
    while(i != f->fim){
        sprintf(elemento,"%d", f->vetor[i]);
        strcat(str, elemento);
        if ((i+1)%f->tamVetor != f->fim) strcat(str, ",");
        i = (i+1) % f->tamVetor;
    }
    strcat(str, "]");
    return true;
}
bool fila_inserirTodos(Fila* f, TipoElemento* vetor, int tamVetor){
    if(!fila_ehValida(f)) return false;

    int i;
    for(i=0; i < tamVetor ; i++){
        fila_inserir(f, vetor[i]);
    }
    return true;
}

