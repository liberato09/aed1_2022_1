#include <stdio.h>
#include <stdlib.h>

int main(int numParametros, char const *parametros[]){
    int a, b;
    
    // ENTRADA
    a = atoi(parametros[1]);
    b = atoi(parametros[2]);
    
    // PROCESSAMENTO
    int soma = a + b;

    // SAÍDA
    printf("Numero parametros: %d\n", numParametros);
    printf("parametro 0: %s\n", parametros[0]);
    printf("parametro 1: %s\n", parametros[1]);
    printf("parametro 2: %s\n", parametros[2]);
    printf("%d\n", soma);

    return 0;
}
