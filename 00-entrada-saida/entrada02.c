#include <stdio.h>

int main(){
    int a, b;

    // ENTRADA
    a = 10;
    b = 20;
    
    // PROCESSAMENTO
    int soma = a + b;

    // SAÍDA
    printf("%d\n", soma);

    return 0;
}
 