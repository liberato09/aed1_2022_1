#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>

int main(){
    int i;
    float f;
    double d;
    char c;

    int *pi;
    float *pf;
    double *pd;
    char *pc;

    printf("sizeof(i): %ld\n", sizeof(i));
    printf("sizeof(f): %ld\n", sizeof(f));
    printf("sizeof(d): %ld\n", sizeof(d));
    printf("sizeof(c): %ld\n\n", sizeof(c));
    printf("sizeof(pi): %ld\n", sizeof(pi));
    printf("sizeof(pf): %ld\n", sizeof(pf));
    printf("sizeof(pd): %ld\n", sizeof(pd));
    printf("sizeof(pc): %ld\n", sizeof(pc));
    printf("sizeof(*pc): %ld\n", sizeof(*pc));
    printf("sizeof(*pi): %ld\n", sizeof(*pi));


    return 0;
}