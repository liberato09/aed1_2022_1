#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>

int main(){

    int v[3] = {10, 20, 30};
    
    int* p;
    p = v;

    void* generico = v;

    printf("v: %u\n", v);
    printf("p: %u\n", p);
    printf("generico: %u\n", generico);

    printf("\n");
    for (int i=0; i < 3; i++){
        printf("v[%d]: %d\n", i, v[i]);
        printf("p[%d]: %d\n", i, p[i]);
        printf("*(p+%d): %d\n", i, *(p+i));
        printf("*(v+%d): %d\n", i, *(v+i));
        printf("*((int*)generico+%d): %d\n", i, *((int*)generico+i));
        

        printf("\n");
    }


    return 0;
}